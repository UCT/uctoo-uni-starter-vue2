const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./public/index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false,
  separator: '__', // 兼容小程序，将 : 替换成 __
  theme: {
    // 兼容小程序，将默认配置里带 .和/ 清除
    // 如果有些属性你没有用到，请在 corePlugins 里禁用
    extend: {},
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      blue: colors.blue,
      emerald: colors.emerald,
      indigo: colors.indigo,
      orange: colors.orange,
      yellow: colors.yellow,
    },
    fontSize: {
      'xs': '11px',
      'sm': '12px',
      'base': '14px',
      'lg': '16px',
      'xl': '18px',
      '2xl': '20px',
      '3xl': '24px',
      '4xl': '26px',
      '5xl': '30px',
      '6xl': '35px',
      '7xl': '40px',
    },
    fontWeight: {
      light: 300,
      normal: 400,
      medium: 700,
      lg: 800,
    },
    boxShadow: {
      'md': '0px 8px 20px 0px rgba(196, 198, 201, 0.2)'
    },
    borderWidth: {
      DEFAULT: '1px',
      '0': '0',
      '2': '2px',
      '3': '3px',
      '4': '4px',
      '6': '6px',
    },
    height: {
      full: '100%'
    },
    inset: {},
    screens: {},
    spacing: {
      '1': '6rpx',
      '2': '10rpx',
      '3': '20rpx',
      '4': '30rpx',
      '5': '40rpx',
      '6': '60rpx',
      '7': '80rpx',
      '8': '100rpx',
    },
    translate: {},
    width: {
      '45': '45%',
      '48': '48%',
      '49': '49%',
      '50': '50%',
      '30': '30%',
      '32': '32%',
      '25': '25%',
      '100': '100%',
    }
  },
  variants: {},
  plugins: [],
  corePlugins: {
    // 兼容小程序，将带有 * 选择器的插件禁用
    preflight: false,
    space: false,
    divideColor: false,
    divideOpacity: false,
    divideStyle: false,
    divideWidth: false
  }
}