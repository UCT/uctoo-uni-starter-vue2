import { Model } from '@vuex-orm/core';
import {getClientToken,setClientToken} from '../utils/auth';
//import Permissions from './Permissions';

export default class WechatopenMiniappUsers extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'wechatopen_miniapp_users'

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            user_ids: this.string(''), //关联管理后台users表ID
            appid: this.string(''),
            openid: this.string(''),
            unionid: this.string(''),
            nickname: this.string(''),
            phoneNumber: this.string(''),
            purePhoneNumber: this.string(''),
            countryCode: this.string(''),
            password: this.string(''),
            gender: this.number(''),
            city: this.string(''),
            province: this.string(''),
            country: this.string(''),
            avatarUrl: this.string(''),
            language: this.string(''),
            subscribe: this.number(''),
            subscribe_time: this.string(''),
            remark: this.string(''),
            tagid_list: this.string(''),
            subscribe_scene: this.string(''),
            qr_scene: this.string(''),
            qr_scene_str: this.string(''),
            privilege: this.string(''),
            loginip: this.string(''),
            token: this.string(''),
            status: this.attr(''),
            access_token: this.string(''),
            access_token_overtime: this.string(''),
            session_key: this.string(''),
            verification: this.string(''),
            created_at: this.attr(''),
            updated_at: this.attr(''),
            deleted_at: this.attr(''),
            creator_id: this.attr(''),
        };
    }

    static fetchById(id) {
        return this.api().get(`/users/${id}`);
    }

    //微信小程序获取用户资料
    static async decryptInfo(params) {
        let result = await this.api().post(`/api/miniapplogin/decryptInfo`,params);
        if(result.response.data.code == 10000){
            uni.setStorageSync('userInfo',result.response.data.data);
        }else{
            console.log(result.response.data.message);
        }
        return result;
    }
}
