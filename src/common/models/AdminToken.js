import { Model } from '@vuex-orm/core';
import {getToken,setToken} from "../utils/auth";

export default class AdminToken extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'admin_token'

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            token: this.string('')
        };
    }

    static async adminLogin(data) {
        let result = await this.api().post(`/login`,data);
        if(result.response.data.code == 10000){
            setToken(result.entities.admin_token[0].token);
        }else{
            console.log(result.response.data.message);
        }
        return result;
    }
}
