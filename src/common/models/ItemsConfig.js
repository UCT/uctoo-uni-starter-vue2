import { Model } from '@vuex-orm/core';
import {getToken} from "../utils/auth";

export default class ItemsConfig extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'items_config';

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            type:this.string(''),
            category:this.string(''),
            name: this.string(''),
            value: this.attr(''),
            page_id: this.attr(''),
            schema: this.attr(''),
            uiSchema: this.attr(''),
            formData: this.attr(''),
            errorSchema: this.attr(''),
            formFooter: this.attr(''),
            formProps: this.attr(''),
            deleted_at: this.attr(0),
            nanoid: this.attr(''), //
        };
    }

    static fetchByToken() {
        return this.api().get(`/vueEditorItemsConfig`,{persistBy: 'create' });
    }

    static insertOrUpdateApi(data) {
        return this.api().post(`/vueEditorItemsConfigSaveAll`,data);
    }
	
	static fetchByNanoid(params) {
	    return this.api().get(`/api/vueEditorItemsConfig`,{params: params,persistBy: 'create' });
	}


   /* static mutators () {
        return {
            value (v) {
                console.log('mutators');
               // console.log(value);
                console.log(JSON.parse(v));
                return JSON.parse(v);
            }
        }
    } */
}
