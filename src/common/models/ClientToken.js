import { Model } from '@vuex-orm/core';
import {getClientToken,setClientToken} from "../utils/auth";

export default class ClientToken extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'client_token'

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            token: this.string(''),
            wechat_miniapp_userid: this.number(''),
        };
    }

    static async checkClientToken(data) {
        let result = await this.api().post(`/api/miniapplogin/checktoken`,data,{save:false});
        if(result.response.data.code == 10000){
            console.log(result.response.data);
        }else{
            console.log(result.response.data.message);
        }
        return result;
    }

    static async fetchClientToken(data) {
        let result = await this.api().post(`/api/miniapplogin/wxlogin`,data);
        if(result.response.data.code == 10000){
            setClientToken(result.entities.client_token[0].token);
        }else{
            console.log(result.response.data.message);
        }
        return result;
    }
}
