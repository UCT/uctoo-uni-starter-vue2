import { Model } from '@vuex-orm/core';
import {getToken} from "../utils/auth";
import ItemsConfig from "./ItemsConfig";

export default class VueEditorPages extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'vue_editor_pages';

    static state ()  {
        return {
            fetching: false
        }
    }

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            name: this.string(''), // 页面名称
            type:this.string(''), // 页面类型
            image:this.string(''),  // 页面预览图
            memo:this.string(''), // 备注
            platform:this.string(''), // 适用平台
            page_title:this.string(''), // 页面标题
            page_path:this.string(''), // 页面路径
            user_id:this.number(''), // 关联users表id
            appstore_user_id:this.number(''), // 关联appstore用户id
            status:this.number(''), // 状态
            edit_status:this.number(''), // 编辑状态
            tags:this.string(''), // 分类标签
            description:this.string(''), // 页面说明
            created_at:this.attr(''), // 创建时间
            updated_at: this.attr(''), //更新时间
            deleted_at: this.attr(''), //软删除字段
            creator_id: this.attr(''), //创建人ID
            nanoid: this.attr(''), //唯一ID
            itemsConfig:this.hasMany(ItemsConfig,'page_id')
        };
    }

    static index(data) {
        return this.api().get(`/vueEditorPages`,data);
    }

    static read(id) {
        return this.api().get(`/vueEditorPages/`+ id);
    }

    static insertOrUpdateApi(data) {
        return this.api().post(`/vueEditorPagesSaveAll`,data);
    }

    static selectPage(id) {
        return this.api().post(`/vueEditorPage/selectPage`,{id:id});
    }

    static newPage(data) {
        return this.api().post(`/vueEditorPage/newPage`,data);
    }
}
