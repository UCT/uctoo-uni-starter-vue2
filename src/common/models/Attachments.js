import { Model } from '@vuex-orm/core';
import {getToken} from "../utils/auth";

export default class Attachments extends Model {
    // This is the name used as module name of the Vuex Store.
    static entity = 'attachments';

    // List of all fields (schema) of the post model. `this.attr` is used
    // for the generic field type. The argument is the default value.
    static fields() {
        return {
            id: this.attr(null),
            path:this.string(''),
            url:this.string(''),
            mime_type: this.string(''),
            file_ext: this.attr(''),
            filesize: this.attr(''),
            filename: this.attr(''),
            driver: this.attr(''),
            created_ar: this.attr(''),
            updated_at: this.attr(''),
            deleted_at: this.attr(''),
            creator_id: this.attr(''),
        };
    }

    static fetchByToken(params) {
        return this.api().get(`/attachments`,{headers: { Authorization: 'Bearer ' + getToken()},params: params, persistBy: 'create' });
    }

    /**
     * Get url.
     */
    get src () {
        return `${this.url}`
    }
}
