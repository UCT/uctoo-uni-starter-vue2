export default {
    name: 'global',
    state: {},
    getters: {},
    mutations: {},
    actions: {},
}


/**
 * 另一种使用方式
 * 直接更改，不用管什么commit，再修改state
 */
// export const globalStore = Vue.observable({
//     devices: [],
// });