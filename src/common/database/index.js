import { Database } from '@vuex-orm/core';
import Users from '../models/Users';
import Permissions from '../models/Permissions';
import ItemsConfig from '../models/ItemsConfig';
import VueEditorPages from '../models/VueEditorPages';
import Attachments from '../models/Attachments';
import AdminToken from '../models/AdminToken';
import ClientToken from '../models/ClientToken';
import WechatopenMiniappUsers from '../models/WechatopenMiniappUsers';

const database = new Database();

database.register(Users);
database.register(Permissions);
database.register(ItemsConfig);
database.register(VueEditorPages);
database.register(Attachments);
database.register(AdminToken);
database.register(ClientToken);
database.register(WechatopenMiniappUsers);

export default database;
