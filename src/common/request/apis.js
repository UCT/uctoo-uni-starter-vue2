/**
 * 接口列表文件
 */
export default {

	/**done 初始化 ↓ **/
	init: {
		url: 'minishop/index/init',
		auth: false,
		method: 'GET',
		// desc: '初始化数据',
	},

	/** 上传图片 ↓ **/
	upload: {
		url: 'api/common/upload/image',
		auth: true,
		method: 'POST',
		// desc: '上传',
	},

	/** 上传Base64图片 ↓ **/
	uploadBase64: {
		url: 'index/uploadBase64',
		auth: false,
		method: 'POST',
		// desc: '上传Base64位图片',
	},

	/** 消息订阅模板 ↓ **/
	messageIds: {
		url: 'notification/template',
		auth: true,
		method: 'GET',
		// desc: '订阅消息模板ids',
	},

	/**done 模板信息 ↓ **/
	template: {
		url: 'minishop/index/template',
		auth: false,
		method: 'GET',
		// desc: '模板信息',
	},
	/** 自定义模板页面 ↓ **/
	custom: {
		url: 'minishop/index/custom',
		auth: false,
		method: 'GET',
		// desc: '自定义模板页面',
	},

	/** 直播 ↓ **/
	live: {
		url: 'live',
		auth: false,
		method: 'GET',
		// desc: '直播列表',
	},

	/** 微信Jssdk ↓ **/
	wechat: {
		jssdk: {
			url: 'wechat/jssdk',
			auth: false,
			method: 'POST',
			// desc: '微信Jssdk',
		},
	},

	/** 签到 ↓ **/
	user_sign: {
		index: {
			url: 'user_sign/index',
			auth: true,
			method: 'GET',
			// desc: '签到记录',
		},
		sign: {
			url: 'user_sign/sign',
			auth: true,
			method: 'POST',
			// desc: '签到',
		}
	},

	/** 同步路由 ↓ **/
	dev: {
		asyncLink: {           //done
			url: 'minishop/index/asyncLink',
			auth: false,
			method: 'POST',
			// desc: '路由表',
		},
		asyncDecorateScreenShot: {    //done
			url: 'minishop/index/asyncDecorateScreenShot',
			auth: false,
			method: 'POST',
			// desc: '更新店铺装修截图',
		},
		asyncBannerBgColor: {
			url: 'index/asyncBannerBgColor',
			auth: false,
			method: 'POST',
			// desc: '路由表',
		},
		debug: {     //done
			url: 'minishop/index/debugLog',
			auth: false,
			method: 'POST',
		}
	},

	/** done 富文本  ↓ **/
	richtext: {
		url: 'minishop/index/richtext',
		auth: false,
		method: 'GET',
		// desc: '富文本数据',
	},

	/** 三级分类 ↓ **/
	category: {
		url: 'category',
		auth: false,
		method: 'GET',
		// desc: '三级分类',
	},
	/** 二级分类 ↓ **/
	categoryGoods: {
		url: 'category/goods',
		auth: false,
		method: 'GET',
		// desc: '点餐用',
	},

	/** 积分商城 ↓ **/
	score: {
		list: {
			url: 'score_goods_sku_price/index',
			auth: false,
			method: 'GET',
			// desc: '积分商品列表',
		},
		detail: {
			url: 'score_goods_sku_price/detail',
			auth: false,
			method: 'GET',
			// desc: '积分详情',
		},
	},

	/** 商户 ↓ **/
	store: {
		list: {
			url: 'store/index',
			auth: true,
			method: 'GET',
			// desc: '商户列表，不需要storeId',
		},
		info: {
			url: 'store.store/index',
			auth: true,
			method: 'GET',
			// desc: '商户信息',
		},
		order: {
			url: 'store.order/index',
			auth: true,
			method: 'GET',
			// desc: '商户订单',
		},
		orderDetail: {
			url: 'store.order/detail',
			auth: true,
			method: 'GET',
			// desc: '订单详情',
		},
		orderSend: {
			url: 'store.order/send',
			auth: true,
			method: 'POST',
			// desc: '订单发货',
		},
		orderConfirm: {
			url: 'store.order/confirm',
			auth: true,
			method: 'POST',
			// desc: '核销订单',
		},
	},

	/** 商品评论 ↓ **/     //done
	goods_comment: {
		list: {
			url: 'minishop/goods/comment/lists',
			auth: false,
			method: 'GET',
			// desc: '商品评论列表',
		},
		type: {
			url: 'minishop/goods/comment/type',
			auth: false,
			method: 'GET',
			//bug desc: '商品评论分类',
		},
	},

	/** 商品 ↓ **/
	goods: {
		lists: {
			url: 'minishop/goods/lists',
			auth: false,
			method: 'GET',
			// desc: '商品列表', done
		},
		seckillList: {
			url: 'goods/seckillList',
			auth: false,
			method: 'GET',
			// desc: '秒杀列表',
		},
		activity: {
			url: 'minishop/goods/activity',
			auth: false,
			method: 'GET',
			//  desc: '活动商品', done
		},
		myGroupon: {
			url: 'activity_groupon/myGroupon',
			auth: true,
			method: 'GET',
			// desc: '我的拼团',
		},
		grouponDetail: {
			url: 'activity_groupon/detail',
			auth: true,
			method: 'GET',
			// desc: '拼团详情',
		},
		grouponItem: {
			url: 'activity_groupon/index',
			auth: false,
			method: 'GET',
			// desc: '拼购列表',
		},
		grouponList: {
			url: 'goods/grouponList',
			auth: false,
			method: 'GET',
			// desc: '拼团商品列表',
		},
		detail: {
			url: 'minishop/goods/detail',
			auth: false,
			method: 'GET',
			// desc: '商品详情', done
		},
		favorite: {
			url: 'minishop/goods/favorite',
			auth: true,
			method: 'POST',
			// desc: '商品收藏', done
		},
		favoriteList: {
			url: 'minishop/goods/favoriteList',
			auth: true,
			method: 'GET',
			// desc: '商品收藏列表',
		},
		viewList: {
			url: 'goods/viewList',
			auth: true,
			method: 'GET',
			// desc: '足迹列表',
		},
		viewDelete: {
			url: 'goods/viewDelete',
			auth: true,
			method: 'POST',
			// desc: '删除足迹',
		},
		storeAddress: {
			url: 'goods/store',
			auth: true,
			method: 'GET',
			// desc: '商品支持的自提点',
		},
	},

	/** done 用户 ↓ **/
	user: {
		info: {
			url: 'minishop/user/index',
			auth: true,
			method: 'GET',
			// desc: '用户信息', done
		},

		profile: {
			url: 'minishop/user/profile',
			auth: true,
			method: 'POST',
			// desc: '修改用户信息', done
		},

		changemobile: {
			url: 'minishop/user/changemobile',
			auth: true,
			method: 'POST',
			// desc: '修改手机号',
		},

		changepwd: {
			url: 'user/changepwd',
			auth: true,
			method: 'POST',
			// desc: '修改密码',
		},

		resetpwd: {
			url: 'user/resetpwd',
			auth: false,
			method: 'POST',
			// desc: '重置密码',
		},

		mobileLogin: {
			url: 'user/mobileLogin',
			auth: false,
			method: 'POST',
			// desc: '手机验证码登录',
		},

		accountLogin: {
			url: 'user/accountLogin',
			auth: false,
			method: 'POST',
			// desc: '账号密码登录',
		},

		getWxMiniProgramSessionKey: {
			url: 'user/getWxMiniProgramSessionKey',
			auth: false,
			method: 'POST',
			// desc: '获取用户session_key',  废弃
		},

		wxMiniProgramLogin: {
			url: 'api/miniapplogin/wxlogin',
			auth: false,
			method: 'POST',
			// desc: '微信小程序登录',
		},

		wxOpenPlatformLogin: {
			url: 'user/wxOpenPlatformLogin',
			auth: false,
			method: 'POST',
			// desc: '微信APP登录',
		},

		register: {
			url: 'user/register',
			auth: false,
			method: 'POST',
			// desc: '用户注册',
		},

		forgot: {
			url: 'user/forgot',
			auth: false,
			method: 'POST',
			// desc: '忘记密码',
		},
		wxMiniProgramChecktoken: {
			url: 'api/miniapplogin/checktoken',
			auth: false,
			method: 'POST',
			// desc: '微信小程序登录',
		},
		wxMiniProgramOauth: {
			url: "api/miniapplogin/decryptInfo",
			auth: false,
			method: "POST",
			// desc: '微信小程序获取用户资料',
		},
	},

	/** 分享 ↓ **/
	share: {
		add: {
			url: 'share/add',
			auth: false,
			method: 'POST',
			// desc: '添加分享记录',
		}
	},

	/** 位置 ↓ **/
	address: {
		area: {
			url: 'minishop/address/area',
			auth: false,
			method: 'GET',
			// desc: '省市区',
		},
		list: {
			url: 'minishop/address/indexlist',
			auth: true,
			method: 'GET',
			// desc: '地址列表',
		},
		edit: {
			url: 'minishop/address/edit',
			auth: true,
			method: 'POST',
			// desc: '修改地址',
		},
		defaults: {
			url: 'minishop/address/defaults',
			auth: true,
			method: 'GET',
			// desc: '默认地址', done
		},
		info: {
			url: 'minishop/address/info',
			auth: true,
			method: 'GET',
			// desc: '地址详情',
		},
		del: {
			url: 'minishop/address/del',
			auth: true,
			method: 'POST',
			// desc: '删除',
		},
	},

	/** 短信 ↓ **/
	sms: {
		send: {
			url: 'api/Dysmsapi/SendSms',
			auth: false,
			method: 'POST',
			// desc: '发送短信',
		},
	},

	/** 常见问题 ↓ **/
	faq: {
		list: {
			url: 'faq',
			auth: false,
			method: 'GET',
			// desc: '常见问题列表',
		},
	},

	/** 意见反馈 ↓ **/
	feedback: {
		type: {
			url: 'feedback/type',
			auth: true,
			method: 'GET',
			// desc: '意见反馈类型',
		},
		add: {
			url: 'feedback/add',
			auth: true,
			method: 'POST',
			// desc: '提交意见',
		},
	},

	/** 购物车 ↓ **/
	cart: {
		index: {
			url: 'minishop/cart/info',
			auth: true,
			method: 'POST',
			// desc: '购物车商品列表', done todo
		},
		add: {
			url: 'minishop/cart/add',
			auth: true,
			method: 'POST',
			// desc: '添加购物车',done todo
		},

		edit: {
			url: 'minishop/cart/edit',
			auth: true,
			method: 'POST',
			// desc: '编辑购物车',done todo
		},
	},

	/** 订单 ↓ **/
	order: {
		index: {
			url: 'minishop/order/indexList',
			auth: true,
			method: 'GET',
			// desc: '订单列表',
		},
		pre: {
			url: 'minishop/order/pre',
			auth: true,
			method: 'POST',
			// desc: '预备提交订单', done
		},
		createOrder: {
			url: 'minishop/order/createOrder',
			auth: true,
			method: 'POST',
			// desc: '提交订单', done
		},
		detail: {
			url: 'minishop/order/detail',
			auth: true,
			method: 'GET',
			// desc: '订单详情',  done
		},
		itemDetail: {
			url: 'minishop/order/itemDetail',
			auth: true,
			method: 'GET',
			// desc: '订单商品详情',
		},
		confirm: {
			url: 'minishop/order/confirm',
			auth: true,
			method: 'POST',
			// desc: '确认收货', done
		},
		refund: {
			url: 'order/refund',
			auth: true,
			method: 'POST',
			// desc: '申请退款',
		},
		cancel: {
			url: 'minishop/order/cancel',
			auth: true,
			method: 'POST',
			// desc: '取消订单', done
		},
		statusNum: {
			url: 'minishop/order/statusNum',
			auth: true,
			method: 'GET',
			// desc: '订单dot', done
		},
		comment: {
			url: 'order/comment',
			auth: true,
			method: 'POST',
			// desc: '评价商品',
		},
		coupons: {
			url: 'minishop/order/coupons',
			auth: true,
			method: 'POST',
			// desc: '商品可用优惠券', done todo
		},
		aftersale: {
			url: 'order_aftersale/aftersale',
			auth: true,
			method: 'POST',
			// desc: '申请售后',
		},
		aftersaleList: {
			url: 'order_aftersale/index',
			auth: true,
			method: 'GET',
			// desc: '售后列表',
		},
		aftersaleDetail: {
			url: 'order_aftersale/detail',
			auth: true,
			method: 'GET',
			// desc: '售后列表详情',
		},
		deleteOrder: {
			url: 'minishop/order/delete',
			auth: true,
			method: 'POST',
			// desc: '删除订单', done
		},
		deleteAftersaleOrder: {
			url: 'order_aftersale/delete',
			auth: true,
			method: 'POST',
			// desc: '删除售后订单',
		},
		cancelAftersaleOrder: {
			url: 'order_aftersale/cancel',
			auth: true,
			method: 'POST',
			// desc: '取消售后订单',
		},
		expressList: {
			url: 'order_express/index',
			auth: true,
			method: 'GET',
			// desc: '包裹列表',
		},
		expressDetail: {
			url: 'order_express/detail',
			auth: true,
			method: 'GET',
			// desc: '包裹详情',
		},
	},

	/** 支付 ↓ **/
	pay: {
		prepay: {
			url: 'minishop/pay/prepay',
			auth: true,
			method: 'POST',
			// desc: '发起支付', done todo 优化支付中间件
		},
	},

	/** 提现 ↓ **/
	user_wallet_apply: {
		apply: {
			url: 'user_wallet_apply/apply',
			auth: true,
			method: 'POST',
			// desc: '申请提现',
		},
		rule: {
			url: 'user_wallet_apply/rule',
			auth: true,
			method: 'GET',
			// desc: '体现规则',
		}

	},

	/** 钱包明细 ↓ **/
	user_wallet_log: {
		url: 'user_wallet_log',
		auth: true,
		method: 'GET',
		// desc: '钱包明细',
	},

	/** 银行卡 ↓ **/
	user_bank: {
		info: {
			url: 'user_bank/info',
			auth: true,
			method: 'GET',
			// desc: '银行卡信息',
		},
		edit: {
			url: 'user_bank/edit',
			auth: true,
			method: 'POST',
			// desc: '编辑银行卡信息',
		}
	},

	/** 评论 ↓ **/
	comment: {
		submit: {
			url: 'comment/submit',
			auth: true,
			method: 'POST',
			// desc: '提交评论',
		},
		list: {
			url: 'comment/list',
			auth: true,
			method: 'GET',
			// desc: '评论列表',
		}
	},

	/** 优惠券 ↓ **/
	coupons: {
		list: {
			url: 'coupons',
			auth: true,
			method: 'GET',
			// desc: '个人中心优惠券列表',
		},
		lists: {
			url: 'minishop/coupons/lists',
			auth: false,
			method: 'GET',
			// desc: '首页优惠券',
		},
		get: {
			url: 'coupons/get',
			auth: true,
			method: 'GET',
			// desc: '领取',
		},
		detail: {
			url: 'coupons/detail',
			auth: true,
			method: 'GET',
			// desc: '购物券详情',
		},
		goods: {
			url: 'coupons/goods',
			auth: true,
			method: 'GET',
			// desc: '适用商品',
		}
	},


};
