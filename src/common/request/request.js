import axios from 'axios';
import axiosAdapterUniapp from 'axios-adapter-uniapp'
import qs from 'qs';
import { API_URL} from './env';

// axios请求超时重新请求次数和时间间隙
//重新请求次数
axios.defaults.retry = 1;
//间隙
axios.defaults.retryDelay = 1000;

// create an axios instance
const service = axios.create({
	baseURL: API_URL,
	timeout: 6000,  //6s超时 重新请求一次
	crossDomain: true,
	sslVerify:false,
    adapter: axiosAdapterUniapp,
});

// request拦截器
service.interceptors.request.use();

// respone拦截器
service.interceptors.request.use();

export default service