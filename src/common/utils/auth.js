const AdminTokenKey = 'AdminToken';
export function getToken() {
    return uni.getStorageSync(AdminTokenKey);
}

export function setToken(token) {
    return uni.setStorageSync(AdminTokenKey,token);
}

export function removeToken() {
    return uni.removeStorageSync(AdminTokenKey);
}

const ClientTokenKey = 'ClientToken';

export function getClientToken() {
    return uni.getStorageSync(ClientTokenKey);
}

export function setClientToken(token) {
    return uni.setStorageSync(ClientTokenKey,token);
}

export function removeClientToken() {
    return uni.removeStorageSync(ClientTokenKey);
}