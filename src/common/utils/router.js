/**
 * 路由守卫
 */
const methodToPatch = ['navigateTo', 'redirectTo', 'switchTab', 'navigateBack']
methodToPatch.map(item => {
    const original = uni[item] // 
    uni[item] = function (opt = {}, needAuth) {
        if (needAuth && !store.getters.token) { // 需要登录且未登录
            uni.navigateTo({
                url: '/pages/common/login/login'
            })
        } else {
            return original.call(this, opt)
        }
    }
})
