export default {
	get(key) {
		return uni.getStorageInfoSync(key)
	},
	set(key, value) {
		uni.setStorageSync(key, value)
	},
	remove(key) {
		uni.removeStorageSync(key)
		
	},
	clear() {
		uni.clearStorageSync()
	},
}