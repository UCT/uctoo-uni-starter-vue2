const i18nMixin = {
	computed: {
		lang() {
			const locale = this._i18n.locale
			switch(locale) {
				case 'en':
					return 'English'
				case 'zh_CN':
					return '中文'
				default:
					return '未知'
			}
		}
	},
	methods: {
		changeLang() {
			const system_info = uni.getStorageSync('system_info')
			system_info.language === 'en' ? system_info.language = this._i18n.locale = 'zh_CN' : system_info.language = this._i18n.locale = 'en'
			uni.setStorageSync('system_info',system_info)
			uni.reLaunch({
				url: 'home'
			})
			
			uni.setTabBarItem({
				index: 0,
				text: this.$t('tabbar').home
			})
			uni.setTabBarItem({
				index: 1,
				text: this.$t('tabbar').mine
			})
		}
	}
}

export default i18nMixin
