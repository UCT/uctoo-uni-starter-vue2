const navigationMixin = {
    methods: {
        push(url) {
            uni.navigateTo({ url })
            console.log('navigateTo：', url)
        },
        redirect(url) {
            uni.navigateTo({ url })
        },
        reLaunch(url) {
            uni.navigateTo({ url })
        },
        switchTab(url) {
            uni.switchTab({ url })
        },
        back(delta = 1) {
            uni.navigateBack({ delta })
        },
    }
}

export default navigationMixin