export default {
    button: {
    	add: '增加',
    	del: '删除',
    	service: '客服'
    },
	tabbar: {
		'home': '首页',
		'mine': '我的'
	},
	system: {
		'feedback': '意见反馈',
		'version': '系统版本',
		'language': '切换语言',
		'settings': '设置'
	},
	userinfo: {
		'nickname': '昵称',
		'notSet': '未设置',
		'notSpecified': '未绑定',
		'phoneNumber': '手机号码',
		'setNickname': '设置昵称',
		'setNicknamePlaceholder': '默认昵称',
		'bindPhoneNumber': '绑定手机号码',
		'bindOtherLogin': '绑定其他登录方式',
		'navigationBarTitle': '个人资料',
		'wxlogin': '微信登录',
		'pwdlogin': '帐号登录'
	},
	pwdLogin: {
		'pwdLogin': '管理后台帐号密码登录',
		'placeholder': '管理后台帐号',
		'passwordPlaceholder': '密码',
		'login': '登录',
		'forgetPassword': '忘记密码',
		'register': '注册'
	},
	settings: {
		'userInfo': '用户信息',
		'clearTmp': '清缓存',
		'pushServer': '推送服务',
		'fingerPrint': '指纹',
		'facial': '刷脸',
		'deactivate': '未激活',
		'logOut': '退出登录',
		'login': '登录',
		'please': '请',
		'successText': '成功',
		'failTip': '失败',
		'errCode': '错误码',
		'authFailed': '认证失败',
		'deviceNoOpen': '设备未开启',
		'fail': '失败',
		'tips': '提示',
		'exitLogin': '退出登录',
		'cancelText': '取消',
		'confirmText': '确定',
		'clearing': '清空',
		'clearedSuccessed': '已清空'
	},
}