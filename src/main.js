import Vue from 'vue'
import App from './App'
import store from './common/store'
import api from './common/request'
//import storage from './common/utils/storage'
import './common/utils/uview'
import './common/utils/router'

import i18n from './lang/index' 



Vue.config.productionTip = false;

App.mpType = 'app';

Vue.prototype.$api = api
//Vue.prototype.$storage = storage
Vue.prototype._i18n = i18n;

const app = new Vue({
  i18n,
  store,
  ...App
});
app.$mount();
