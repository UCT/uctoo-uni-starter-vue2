module.exports = {
    transpileDependencies: ['uview-ui'],
    devServer: {
        proxy: {
            '/wp-json': {
                target: 'http://localhost/wordpress',
                secure: false,
                changeOrigin: true
            }
        }
    }
}