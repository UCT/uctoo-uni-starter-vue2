# uctoo-uni-starter-vue2

#### 简介
uctoo-uni-starter基础脚手架，实现了大量商用项目常见功能，支持可视化页面搭建，云端一体应用快速开发基本项目模版。

APP有很多通用的功能，比如登录注册、头像、设置、banner、... uctoo-uni-starter将这些功能都已经集成好。并且支持使用[https://gitee.com/UCT/uctoo-vue-editor](https://gitee.com/UCT/uctoo-vue-editor) 进行可视化页面搭建。  

项目采用了全栈模型同构的技术选型，使得前后端算法基本保持一致，为低代码、无代码、可视化代码生成等开发需求提供了更加规范化的架构风格和高效率的开发基础设施。  

虽然近年来采用文档型数据库的云开发技术选型愈加流行，但实际业务应用中有大量需要融合或集成已有关系型数据库等ICT基础设施的需求，本项目在兼容原有技术选型的基础上又提供了与云开发一致的开发体验。  

## 基础脚手架构成

1. [uview2.0](https://www.uviewui.com/components/intro.html)，基础ui库
2. [tailwindcss](https://tailwindcss.com/)，超级好用的工具类css库
3. 全家桶，vue-router，vuex等
4. 封装全局请求，兼容中心化API接口文件的传统编程模式。
5. easycom自动引入组件示例
6. 封装通用存储
7. 集成[vuex-orm](https://github.com/vuex-orm/vuex-orm)、[Vuex ORM Axios](https://github.com/vuex-orm/plugin-axios),示例了全栈模型同构的编程模型，弱化API通信的存在感，简化业务逻辑算法，支持采用关系型数据库的项目与采用文档型数据库的项目有相似的前端操作数据库编程模型。  
8. 首页示例了通过API方式与[https://gitee.com/UCT/uctoo-vue-editor](https://gitee.com/UCT/uctoo-vue-editor) 页面可视化装修模块集成的方式。  
9. 支持国际化多语言

## 项目运行

## 在hbuildx中运行

1. 需要先使用 `npm install` 安装下所有包
2. 重启hbuildx编辑器
3. 然后在hbuildx中，打开根目录，点击左上角运行，选择h5还是小程序运行就ok

## 在vscode等编辑器中运行

1. 和正常脚手架运行一样，先 `npm install` 安装包
2. 然后看 `package.json` 下的script字段，有哪些命令，如运行h5，`npm run dev:h5`

### uctoo-uni-starter集成包括：
1. 用户管理：
	- 登录注册（用户名密码登录、手机号验证码登录、APP一键登录、微信登录、Apple登录、微信小程序登录）
	- 修改密码、忘记密码、头像更换（集成图片裁剪）、昵称修改、积分查看、退出登录
	- APP、H5、小程序多端统一用户中心及权限体系
2. 系统设置：
	- App更新（整包升级、wgt升级、强制升级，后台搭配uctoo admin的应用中心管理）
	- 推送开关（app）、清除缓存（app）
	- 指纹解锁（app）、人脸解锁（app）
	- 多语言切换
	- 账号注销（正在完善中...）
	
* 部分页面由uni-starter项目改编，更换为对接UCToo服务端API，更多功能模块会不断更新，请持续关注本项目 

## 目录结构@catalogue
<pre>
uctoo-uni-starter
├─common	公共模块
│	├─database 前端数据库目录
│	|	└─index.js 前端数据库定义
│	├─mixin	mixin目录
│	│	├─i18n.js			多语言支持
│	│	└─navigation.js	页面跳转
│	├─models				前端数据库模型目录,前端数据库与后台API通信的接口都包含在各模型中
│	│	├─AdminToken.js	管理后台帐号登录后的token，对应users表的remember_token字段，请求管理后台API时，需在请求头添加此token验证接口权限。
│	│	├─Attachments.js	附件
│	│	├─ClientToken.js	前端用户帐号登录后的token，对应wechatopen_miniapp_users等多端用户表的token字段，请求前端API时，需在请求头添加此token验证接口权限。
│	│	├─ItemsConfig.js	 可视化页面配置数据模型
│	│	├─Permissions.js	 权限模型，兼容多态用户组及权限
│	│	├─Users.js	     管理后台用户模型
│	│	├─VueEditorPages.js		可视化配置页面模型
│	│	└─WechatopenMiniappUsers.js	微信小程序用户模型
│	├─request						
│	│	├─apis.js						与模型关联不紧密的API接口，或者无对应的前端数据库模型的API可以仍采用中心化的API请求机制
│	│	├─env.js					环境配置
│	│	├─index.js				中心化API请求的封装
│	│	├─request.js				axios适配uniapp请求的封装，用于集成[Vuex ORM Axios](https://github.com/vuex-orm/plugin-axios),即models中的API通信都用此入口
│	│	└─request2.js				uniapp请求的封装，即apis.js中的API通信都用此入口
│	├─router
│	│	└─index.js	 					路由
│	├─services                   服务目录
│	│	└─auth-header.js			自动在所有axios请求头添加权限token
│	├─store
│	│	├─modules							兼容传统vuex使用方式的模块，可自动加载
│	│	│	└─global.js                   全局存储
│	│	└─index.js						集成[vuex-orm]、[Vuex ORM Axios]以前端数据库的模式使用存储
│	├─utils					     工具目录
│	│	├─auth.js                  封装token存储
│	│	├─router.js				路由守卫（暂未使用）
│	│	├─storage.js              缓存封装（暂未使用）	
│	│	└─uview.js                集成uview
├─components	组件目录					
├─lang	    多语言目录	
├─pages	    页面文件目录	
│	├─common  通用页面文件目录		
│	├─home    首页目录		
│	│	└─home.vue					首页，示例了采用API方式集成页面可视化搭建系统
│	└─ucenter.vue						统一用户中心
│	│	├─login-page                  登录相关页面目录(待完善)
│	│	└─settings.vue				设置相关页面目录(待完善)
│	│	├─userinfo					用户个人信息目录(待完善)
│	│	│	├─bind-mobile
│	│	│	│	└─bind-mobile.vue			绑定手机号码
│	│	│	├─limeClipper					图片裁剪插件,来源[limeClipper](https://ext.dcloud.net.cn/plugin?id=3594) @作者： 陌上华年
│	│	│	│	├─images
│	│	│	│	│	├─photo.svg
│	│	│	│	│	└─rotate.svg
│	│	│	│	├─index.css
│	│	│	│	├─limeClipper.vue
│	│	│	│	├─README.md
│	│	│	│	└─utils.js
│	│	│	└─ucenter.vue             统一用户中心
│	|
├─static	 						存放应用引用的本地静态资源（如图片、视频等）的目录，<b>注意：</b>静态资源只能存放于此
├─App.vue							应用配置，用来配置App全局样式以及监听 <a href="/collocation/frame/lifecycle?id=应用生命周期">应用生命周期</a>
├─main.js							Vue初始化入口文件
├─manifest.json	 					配置应用名称、appid、logo、版本等打包信息，<a href="/collocation/manifest">详见</a>
└─pages.json						配置页面路由、导航条、选项卡等页面类信息，<a href="/collocation/pages">详见</a>
</pre>

## Roadmap  
1. 与uniapp的采用云开发的uni-starter项目功能需求路标对齐

#### 参考资料
1. [UCToo uni starter 移动端起始项目脚手架开源](https://mp.weixin.qq.com/s?__biz=MjM5MzMxMzYyNg==&mid=2649941679&idx=1&sn=4aa05b06b756b26232d519d11d87b0d0&chksm=be9ea09a89e9298ca0b332d9195d4a6a15b44dd337715d14e58de5cce414d5f85f2f344279db&token=1225429042&lang=zh_CN#rd)